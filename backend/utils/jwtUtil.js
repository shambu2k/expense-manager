require('dotenv').config();
const jwt_secret = process.env.JWT_SECRET;
const jwt = require('jsonwebtoken');

exports.verifyToken = async (req, res, next) => {
	const bearerHeader = req.headers['authorization'];
	if (typeof bearerHeader !== 'undefined') {
		const bearerToken = bearerHeader.split(' ')[1];
		jwt.verify(bearerToken, jwt_secret, function (err, data) {
			if (err) {
				res.status(401).json({message: 'Unauthenticated'});
			} else if (data.user_stuff.email !== req.body.email) {
				res.status(401).json({message: 'User token mismatch'});
			} else {
				next();
			}
		});
	} else {
		res.status(400).json({message: 'Bad request'});
	}
};

exports.generateToken = async (user_stuff) => {
	return jwt.sign({user_stuff}, jwt_secret);
};
