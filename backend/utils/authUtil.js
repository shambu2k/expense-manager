require('dotenv').config();

const {Deta} = require('deta');
const deta = Deta(process.env.DETA_KEY);
const dbUsers = deta.Base('users');

const crypto = require('crypto');

exports.hashPassword = (password) => {
	const salt = crypto.randomBytes(64).toString('hex');
	const password_hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512'); //returns hashed password
	const hash = {
		password_hash: password_hash,
		salt: salt,
	};
	return hash;
};

exports.checkPassword = (password_hash, salt, password) => {
	return password_hash == crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');
};

exports.checkIfAdmin = async (req, res, next) => {
	const adminEmail = req.body.email;
	if (adminEmail !== undefined) {
		const adminUser = await dbUsers.get(adminEmail);
		if (adminUser.isAdmin) {
			next();
		} else {
			res.status(403).json({message: 'Forbidden, requires admin privileges'});
		}
	} else {
		res.status(400).json({message: 'Bad request'});
	}
};
