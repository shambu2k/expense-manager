exports.getMonth = async (dateString) => {
	return parseInt(dateString.substring(5, 7));
};
// return parseInt(new Date().toISOString().split("T")[0].substring(5, 7));

exports.getYear = async (dateString) => {
	return parseInt(dateString.substring(0, 4));
};

exports.filterMonthExpenses = async (expenses, month, year) => {
	let monthExpenses = [];
	for (const expense of expenses) {
		if (parseInt(expense.datetime.substring(5, 7)) == month && parseInt(expense.datetime.substring(0, 4)) == year) {
			monthExpenses.push(expense);
		}
	}
	return monthExpenses;
};

exports.getWeeklyExpenses = async (monthExpenses) => {
	let weeklyExpenses = [0, 0, 0, 0];
	for (const expense of monthExpenses) {
		const date = parseInt(expense.datetime.substring(8, 10));
		if (date > 0 && date <= 31) {
			if (date > 28) {
				weeklyExpenses[3] += expense.amount;
			} else {
				weeklyExpenses[parseInt((date - 1) / 7)] += expense.amount;
			}
		}
	}
	return weeklyExpenses;
};

exports.getCategoryAmount = async (monthExpenses) => {
	let categoryExpenses = [0, 0, 0, 0, 0];
	for (const expense of monthExpenses) {
		categoryExpenses[expense.category] += expense.amount;
	}
	return categoryExpenses;
};
