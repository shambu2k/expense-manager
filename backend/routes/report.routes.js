const reportRouter = require('express').Router();
const jwtUtil = require('../utils/jwtUtil');
const reportController = require('../controllers/report.controller');

reportRouter.post('/reports', jwtUtil.verifyToken, reportController.getReport);

module.exports = reportRouter;
