const adminRouter = require('express').Router();
const jwtUtil = require('../utils/jwtUtil');
const authUtil = require('../utils/authUtil');
const adminController = require('../controllers/admin.controller');

adminRouter.post('/allUsers', jwtUtil.verifyToken, authUtil.checkIfAdmin, adminController.getAllUsers);
adminRouter.post('/userExpenses', jwtUtil.verifyToken, authUtil.checkIfAdmin, adminController.getUserExpenses);

module.exports = adminRouter;
