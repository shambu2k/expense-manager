const expenseRouter = require('express').Router();
const jwtUtil = require('../utils/jwtUtil');
const expenseController = require('../controllers/expense.controller');

expenseRouter.post('/expenses', jwtUtil.verifyToken, expenseController.getExpenses);
expenseRouter.post('/latestExpenses', jwtUtil.verifyToken, expenseController.getLast5Expenses);
expenseRouter.post('/addExpense', jwtUtil.verifyToken, expenseController.addExpense);
expenseRouter.post('/updateExpense', jwtUtil.verifyToken, expenseController.updateExpense);
expenseRouter.post('/deleteExpense', jwtUtil.verifyToken, expenseController.deleteExpense);

module.exports = expenseRouter;
