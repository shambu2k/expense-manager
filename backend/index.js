require('dotenv').config();
const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());

// Routes
app.get('/', (req, res) => {
	res.send('Deployed on deta');
});
app.use('/api', require('./routes/user.routes'));
app.use('/api', require('./routes/expense.routes'));
app.use('/api', require('./routes/report.routes'));
app.use('/api', require('./routes/admin.routes'));

app.listen(process.env.PORT, () => {
	console.log(`Server listening at port ${process.env.PORT}...`);
});

module.exports = app;
