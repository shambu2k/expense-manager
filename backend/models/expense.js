const categories = ['Home', 'Food', 'Fuel', 'Shopping', 'Other'];

class Expense {
	email = '';
	datetime = '';
	currency = 'INR';
	amount = 0;
	category = 0;
	description = '';
	constructor(email, datetime, currency, amount, category, description = '', expensesId = []) {
		this.email = email;
		this.datetime = datetime;
		this.currency = currency;
		this.amount = amount;
		this.category = category;
		this.description = description;
	}
}

module.exports = Expense;
