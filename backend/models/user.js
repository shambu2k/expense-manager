class User {
	name = '';
	email = '';
	password_hash = '';
	salt = '';
	token = '';
	isAdmin = false;
	expensesId = [];
	total = 0;
	constructor(name, email, password_hash, salt, token = '', isAdmin = false, expensesId = [], total = 0) {
		this.name = name;
		this.email = email;
		this.password_hash = password_hash;
		this.salt = salt;
		this.token = token;
		this.isAdmin = isAdmin;
		this.expensesId = expensesId;
		this.total = total;
	}
}

module.exports = User;
