require('dotenv').config();

class Test {
	constructor(name, list) {
		this.name = name;
		this.list = list;
	}
}

async function aa() {
	const {Deta} = require('deta');
	const deta = Deta(process.env.DETA_KEY);
	const db = deta.Base('users');
	let test = new Test();
	let items = await db.put({key: 'y091y22fhwap', name: '4', list: [1, 2, 4]}, 'y091y22fhwap');
	console.log(items);
}

aa();
