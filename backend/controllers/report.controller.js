require('dotenv').config();
const reportUtil = require('../utils/reportUtil');

const {Deta} = require('deta');
const deta = Deta(process.env.DETA_KEY);
const dbExpenses = deta.Base('expenses');

exports.getReport = async (req, res, next) => {
	try {
		const currentDateString = new Date().toISOString().split('T')[0];
		const currentMonth = await reportUtil.getMonth(currentDateString);
		const currentYear = await reportUtil.getYear(currentDateString);
		const expenses = await dbExpenses.fetch({email: req.body.email});
		const monthlyExpenses = await reportUtil.filterMonthExpenses(expenses.items, currentMonth, currentYear);
		const weeklyAmount = await reportUtil.getWeeklyExpenses(monthlyExpenses);
		const categoryAmount = await reportUtil.getCategoryAmount(monthlyExpenses);
		return res.status(200).json({weekLine: weeklyAmount, catPie: categoryAmount});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};
