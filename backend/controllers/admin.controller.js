require('dotenv').config();

const {Deta} = require('deta');
const deta = Deta(process.env.DETA_KEY);
const dbUsers = deta.Base('users');
const dbExpenses = deta.Base('expenses');

exports.getAllUsers = async (req, res, next) => {
	try {
		const result = await dbUsers.fetch();
		// Filter out password hashes and tokens
		let users = [];
		result.items.forEach((user) => {
			users.push({
				email: user.email,
				isAdmin: user.isAdmin,
				total: user.total,
				name: user.name,
			});
		});
		return res.status(200).json({users: users});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};

exports.getUserExpenses = async (req, res, next) => {
	try {
		let expenses = await dbExpenses.fetch({email: req.body.userEmail});
		return res.status(200).json({expenses: expenses.items});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};
