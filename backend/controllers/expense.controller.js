require('dotenv').config();
const Expense = require('../models/expense');

const {Deta} = require('deta');
const deta = Deta(process.env.DETA_KEY);
const dbExpenses = deta.Base('expenses');
const dbUsers = deta.Base('users');

// TODO: Paginated requests
exports.getExpenses = async (req, res, next) => {
	try {
		let expenses = await dbExpenses.fetch({email: req.body.email});
		return res.status(200).json({expenses: expenses.items});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};

// Gives last 5 added expenses
exports.getLast5Expenses = async (req, res, next) => {
	try {
		const user = await dbUsers.get(req.body.email);
		const expenses = user.expensesId.slice(Math.max(user.expensesId.length - 5, 0));
		let result = [];
		for (let i = expenses.length - 1; i >= 0; i--) {
			const expense = await dbExpenses.get(expenses[i]);
			result.push(expense);
		}
		return res.status(200).json({total: user.total, expenses: result});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};

exports.addExpense = async (req, res, next) => {
	const expense = new Expense(
		req.body.email,
		req.body.datetime,
		req.body.currency,
		req.body.amount,
		req.body.category,
		req.body.description
	);
	try {
		const response = await dbExpenses.put(expense);
		let user = await dbUsers.get(req.body.email);
		user.expensesId.push(response.key);
		user.total += expense.amount;
		await dbUsers.put(user, user.email);
		return res.status(200).json({message: 'Success'});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};

// Expects expense Id from request and all other fields
exports.updateExpense = async (req, res, next) => {
	const expense = new Expense(
		req.body.email,
		req.body.datetime,
		req.body.currency,
		req.body.amount,
		req.body.category,
		req.body.description
	);
	try {
		const oldExpense = await dbExpenses.get(req.body.expenseId);
		await dbExpenses.put(expense, req.body.expenseId);
		let user = await dbUsers.get(req.body.email);
		user.total += expense.amount - oldExpense.amount;
		await dbUsers.put(user, user.email);
		return res.status(200).json({message: 'Success'});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};

// Expects expense Id and amount from request
exports.deleteExpense = async (req, res, next) => {
	try {
		await dbExpenses.delete(req.body.expenseId);
		let user = await dbUsers.get(req.body.email);
		const index = user.expensesId.indexOf(req.body.expenseId);
		if (index > -1) {
			user.expensesId.splice(index, 1);
		}
		user.total -= req.body.amount;
		await dbUsers.put(user, req.body.email);
		return res.status(200).json({message: 'Success'});
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};
