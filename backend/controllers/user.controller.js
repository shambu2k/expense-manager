require('dotenv').config();
const jwtUtil = require('../utils/jwtUtil');
const authUtil = require('../utils/authUtil');
const User = require('../models/user');

const {Deta} = require('deta');
const deta = Deta(process.env.DETA_KEY);
const dbUsers = deta.Base('users');

exports.signUp = async (req, res, next) => {
	try {
		const hash = await authUtil.hashPassword(req.body.password);
		let user = new User(req.body.name, req.body.email, hash.password_hash.toString('hex'), hash.salt);
		token = await jwtUtil.generateToken({email: user.email, salt: user.salt});
		user.token = token;
		if (!(await dbUsers.get(user.email))) {
			await dbUsers.put(user, user.email);
			return res.status(200).json(user);
		} else {
			return res.status(409).json({message: 'User already exists, please login'});
		}
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};

exports.login = async (req, res, next) => {
	try {
		const user = await dbUsers.get(req.body.email);
		console.log(user);
		if (user !== null) {
			if (authUtil.checkPassword(user.password_hash, user.salt, req.body.password)) {
				return res
					.status(200)
					.json({name: user.name, email: user.email, token: user.token, isAdmin: user.isAdmin});
			} else {
				return res.status(401).json({message: 'Invalid password'});
			}
		} else {
			return res.status(404).json({message: 'User does not exist, please signup'});
		}
	} catch (err) {
		console.log(err);
		return res.status(500).json({message: 'Internal Server Error'});
	}
};
