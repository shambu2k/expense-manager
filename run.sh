#!/bin/bash
VAR1=$1

if [[ $VAR1 == "dev" ]]; then
    cd $PWD/backend && npm start &
    cd $PWD/frontend && npm run serve
elif [[ $VAR1 == "lint" ]]; then
    cd $PWD/frontend && npm run lint && cd ../backend && npm run lint
elif [[ $VAR1 == "install" ]]; then
    cd $PWD/frontend && npm i && cd ../backend && npm i
else
    echo "dev/lint/install are the only accepted args!"
fi
