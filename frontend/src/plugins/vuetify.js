import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
Vue.use(Vuetify);

export default new Vuetify({
  iconfont: "md",
  theme: {
    themes: {
      light: {
        primary: "#5e81ac",
        secondary: "#88c0d0",
        accent: "#ebcb8b",
        base: "#d8dee9",
      },
      dark: {
        primary: "#5e81ac",
        secondary: "#88c0d0",
        accent: "#ebcb8b",
        base: "#2e3440",
      },
    },
  },
});
