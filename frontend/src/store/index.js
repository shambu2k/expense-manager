import Vue from "vue";
import Vuex from "vuex";
import ApiService from "../ApiService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: localStorage.getItem("loggedIn") === "true",
    name: localStorage.getItem("name"),
    email: localStorage.getItem("email"),
    token: localStorage.getItem("token"),
    total: 0,
    isAdmin: localStorage.getItem("isAdmin") === "true",
    last5Expenses: [],
    categories: ["Home", "Food", "Fuel", "Shopping", "Other"],
    darkTheme: localStorage.getItem("isDark") === "true",
  },
  mutations: {
    SET_LOGGED_IN(state) {
      state.loggedIn = true;
    },
    SET_LOGGED_OUT(state) {
      state.loggedIn = false;
    },
    SET_USER_NAME(state, name) {
      state.name = name;
    },
    SET_USER_EMAIL(state, email) {
      state.email = email;
    },
    SET_USER_TOKEN(state, token) {
      state.token = token;
    },
    SET_TOTAL(state, total) {
      state.total = total;
    },
    SET_ADMIN(state, isAdmin) {
      state.isAdmin = isAdmin;
    },
    SET_DARK_THEME(state, isDark) {
      state.darkTheme = isDark;
    },
  },
  actions: {
    async setUserInfo(state, user) {
      state.commit("SET_LOGGED_IN");
      state.commit("SET_USER_NAME", user.name);
      state.commit("SET_USER_EMAIL", user.email);
      state.commit("SET_USER_TOKEN", user.token);
      state.commit("SET_ADMIN", user.isAdmin);
      localStorage.setItem("loggedIn", true);
      localStorage.setItem("name", user.name);
      localStorage.setItem("email", user.email);
      localStorage.setItem("token", user.token);
      localStorage.setItem("isAdmin", user.isAdmin);
    },
    async logoutUser(state) {
      state.commit("SET_LOGGED_OUT");
      state.commit("SET_USER_NAME", "");
      state.commit("SET_USER_EMAIL", "");
      state.commit("SET_USER_TOKEN", "");
      state.commit("SET_ADMIN", false);
      localStorage.setItem("loggedIn", false);
      localStorage.setItem("name", "");
      localStorage.setItem("email", "");
      localStorage.setItem("token", "");
      localStorage.setItem("isAdmin", false);
    },
    async updateTotal(state, total) {
      state.commit("SET_TOTAL", total);
    },
    async toggleDarkMode(state, isDark) {
      state.commit("SET_DARK_THEME", isDark);
      localStorage.setItem("isDark", isDark);
    },
    async getLast5Expenses(state) {
      return ApiService.getLast5Expenses(
        { email: state.state.email },
        state.state.token
      );
    },
    async getAllExpenses(state) {
      return ApiService.getExpenses(
        { email: state.state.email },
        state.state.token
      );
    },
    async deleteExpense(state, payload) {
      return ApiService.deleteExpense(
        {
          expenseId: payload.id,
          email: state.state.email,
          amount: payload.amount,
        },
        state.state.token
      );
    },
    async editExpense(state, payload) {
      return ApiService.updateExpense(
        {
          expenseId: payload.id,
          email: state.state.email,
          datetime: payload.datetime,
          currency: payload.currency,
          amount: payload.amount,
          category: payload.category,
          description: payload.description,
        },
        state.state.token
      );
    },
    async addExpense(state, payload) {
      return ApiService.addExpense(
        {
          email: state.state.email,
          datetime: payload.datetime,
          currency: payload.currency,
          amount: payload.amount,
          category: payload.category,
          description: payload.description,
        },
        state.state.token
      );
    },
    async getReports(state) {
      return ApiService.getReports(
        { email: state.state.email },
        state.state.token
      );
    },
    async getAllUsers(state) {
      return ApiService.getAllUsers(
        { email: state.state.email },
        state.state.token
      );
    },
    async getUserExpenses(state, userEmail) {
      return ApiService.getUserExpenses(
        { email: state.state.email, userEmail: userEmail },
        state.state.token
      );
    },
  },
  getters: {
    getLoggedInStatus: (state) => state.loggedIn,
    getUserName: (state) => state.name,
    getUserEmail: (state) => state.email,
    getUserToken: (state) => state.token,
    getUserTotal: (state) => state.total,
    getCategories: (state) => state.categories,
    getIsDark: (state) => state.darkTheme,
    getIsAdmin: (state) => state.isAdmin,
  },
});
