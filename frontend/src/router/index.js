import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Expenses from "../views/Expenses.vue";
import Reports from "../views/Reports.vue";
import SignIn from "../views/SignIn.vue";
import SignUp from "../views/SignUp.vue";
import AdminPage from "../views/AdminPage.vue";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "/signin",
    name: "SignIn",
    component: SignIn,
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/expenses",
    name: "Expenses",
    component: Expenses,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/reports",
    name: "Reports",
    component: Reports,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin",
    name: "AdminPage",
    component: AdminPage,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters.getLoggedInStatus) {
      next({ name: "SignIn" });
    } else if (store.getters.getIsAdmin && to.name !== "AdminPage") {
      next({ name: "AdminPage" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
