const axios = require("axios");
const config = process.env.VUE_APP_API_URL;
export default {
  // data = {'name', 'email', 'password'}
  signup: async (data) => {
    return axios.default.post(config + "/signup", data);
  },
  // data = {'email', 'password'}
  login: async (data) => {
    return axios.default.post(config + "/login", data);
  },
  // data = {'email'}
  getExpenses: async (data, token) => {
    return axios.default.post(config + "/expenses", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'email'}
  getLast5Expenses: async (data, token) => {
    return axios.default.post(config + "/latestExpenses", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'email', ''dataTime', 'currency', 'amount', 'category', 'description'}
  addExpense: async (data, token) => {
    return axios.default.post(config + "/addExpense", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'expenseId', 'email', ''dataTime', 'currency', 'amount', 'category', 'description'}
  updateExpense: async (data, token) => {
    return axios.default.post(config + "/updateExpense", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'expenseId', 'email', 'amount'}
  deleteExpense: async (data, token) => {
    return axios.default.post(config + "/deleteExpense", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'email'}
  getReports: async (data, token) => {
    return axios.default.post(config + "/reports", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'email'}
  getAllUsers: async (data, token) => {
    return axios.default.post(config + "/allUsers", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  // data = {'email', 'userEmail'}
  getUserExpenses: async (data, token) => {
    return axios.default.post(config + "/userExpenses", data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
};
