// vetur.config.js
/** @type {import('vls').VeturConfig} */
module.exports = {
  // **optional** default: `{}`
  // override vscode settings
  // Notice: It only affects the settings used by Vetur.
  settings: {
    "vetur.useWorkspaceDependencies": true,
    "vetur.experimental.templateInterpolationService": true,
  },
  // **optional** default: `[{ root: './' }]`
  // support monorepos
  projects: [
    "./packages/repo2", // Shorthand for specifying only the project root location
    {
      root: "./",
      package: "./package.json",
      globalComponents: ["./src/components/**/*.vue"],
    },
  ],
};
