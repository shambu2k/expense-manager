# Expense Manager

App deployed here - [https://expense-manager-d0e51.web.app/](https://expense-manager-d0e51.web.app/)

#### Setup
Open terminal in project root dir

1. Setup using bash script
```bash
chmod 744 run.sh
./run.sh install
./run.sh dev
```
2. Normal setup
```bash
cd frontend && npm i && cd ../backend && npm i
npm start
```
Open another terminal in project root.
```bash
cd frontend && npm run serve
```

## TODO
### Requirements

The task must satisfy the following requirements:

- [x] The front-end must be a Single-Page-Application.
- [x] Follow proper design guidelines and coding standards.
- [x] User authentication: SignIn and Signup
- [x] Provide three pages - Dashboard, Expenses, and reports accessible through a navbar.

#### Dashboard:
- [x] Show total spent amount
- [x] List last 5 transactions
#### Expenses:
- [x] List of expenses for the current user.
- [x] Button to create a new expense.
- [x] Must contain the following fields: DateTime, Amount, Currency, Description (optional), and a Category.
- [x] The categories are -- Home, Food, Fuel, Shopping and Other.
- [x] Can be created only after logging in
- [x] Can be read only by the user who owns it.
- [x] Can be updated or deleted by the user who owns it.
- [x] Ability to filter the expenses by category.
#### Report:
- [x] Show total amount spent per week of the current month.
- [x] Show total amount spent per category basis on the current month.

### Improvements
- [x] No business logic code in client app. (It must be only on the backend-server)
- [ ] The front-end web client is a PWA (Progressive Web App).
- [x] Hosting the front-end and back-end to a cloud or hosting provider.
    - [ ] Better if the cloud provider is Google Cloud Platform.
- [ ] Using GraphQL instead of traditional REST API.
- [ ] Pagination on the list of expenses.
- [ ] Use ReactiveExtensions in client web app: (RxJs for Javascript)
- [x] User roles -- Admin and normal users.
    - [x] A separate page for Admin listing all users (only accessible by admin users)
    - [x] Admin users can view the expenses of all the users.
    - [ ] Admin users can edit or delete the expense of all users.
